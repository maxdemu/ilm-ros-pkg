#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    counter = 0;
}

void MainWindow::countSlot()
{
	counter++;
	QString s = QString::number(counter);
	this->ui->label->setText(s);
}

MainWindow::~MainWindow()
{
    delete ui;
}

