#pragma once

#include <qobject.h>

class GUIUpdater : public QObject
{
    Q_OBJECT
    
public:
    void updateLabelCount(std::string count)    {emit SIGNAL_updateLabelCount(count);}
    void setUiEnabled(bool enabled)     {emit SIGNAL_setUiEnabled(enabled);}
    
    static GUIUpdater* _updaterInstance;
    
signals:
    void SIGNAL_updateLabelCount(std::string count);
    void SIGNAL_setUiEnabled(bool enabled);
};
