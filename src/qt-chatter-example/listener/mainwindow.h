#pragma once

#include "ros/ros.h"

#include <QMainWindow>

#include "GUIUpdater.h"

#include <atomic>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    
public slots:
    void slotButtonStartPressed();
    void slotButtonStopPressed();
    
    void modSlotUpdateLabelCount(std::string count);
    void modSlotSetUiEnabled(bool enabled);

private:
    Ui::MainWindow *ui;
    GUIUpdater* _guiUpdaterPtr;
    ros::NodeHandle* _nodePtr;
    
    std::atomic<bool> _stop;
};
