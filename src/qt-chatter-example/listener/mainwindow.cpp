#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "ros/ros.h"
#include "std_msgs/String.h"

#include "GUIUpdater.h"

#include <chrono>
#include <thread>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    
    //Setup the GUI updater
    _guiUpdaterPtr = new GUIUpdater();
    GUIUpdater::_updaterInstance = _guiUpdaterPtr;
    
    //Connect GUI updater to MainWIndow slots
    QObject::connect(
    _guiUpdaterPtr, 
    SIGNAL(SIGNAL_updateLabelCount(std::string)), 
    this, 
    SLOT(modSlotUpdateLabelCount(std::string))
    );
    
    QObject::connect(
    _guiUpdaterPtr, 
    SIGNAL(SIGNAL_setUiEnabled(bool)), 
    this, 
    SLOT(modSlotSetUiEnabled(bool))
    );
    
    _stop = false;
    
    //Init ROS
    int zero = 0;
    ros::init(zero, 0, "listener");
    _nodePtr = new ros::NodeHandle();
}

// ################################
// ########## SLOTS ###############
// ################################

//Calback function for ROS
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
    std::cout << "I heard: " << msg->data << std::endl;
    GUIUpdater::_updaterInstance->updateLabelCount(msg->data);
}

//Task for thread
void task(GUIUpdater* guiUpdaterPtr, std::atomic<bool>* stop_ptr, ros::NodeHandle* nodePtr)
{
    using namespace std::chrono_literals;
    guiUpdaterPtr->setUiEnabled(false);
    ros::Subscriber subscriber = nodePtr->subscribe("chatter", 1000, chatterCallback);
    std::cout << "Listening..." << std::endl;
    while((*stop_ptr) == false)
    {
        ros::spinOnce();
    }
    std::cout << "Done listening" << std::endl;
    std::this_thread::sleep_for(1s);
    *stop_ptr = false;
    guiUpdaterPtr->setUiEnabled(true);
}

void MainWindow::slotButtonStartPressed()
{
    std::thread t(task, _guiUpdaterPtr, &_stop, _nodePtr);
    t.detach();
}

void MainWindow::slotButtonStopPressed()
{
    _stop = true;
    ui->pushButtonStop->setEnabled(false);
}

// ################################
// ########## MOD SLOTS ###########
// ################################

void MainWindow::modSlotUpdateLabelCount(std::string count)
{
    ui->labelCount->setText(QString::fromStdString(count));
}

void MainWindow::modSlotSetUiEnabled(bool enabled)
{
    if(enabled)
    {
        ui->pushButtonStart->setEnabled(true);
        ui->pushButtonStop->setEnabled(false);
    }
    else
    {
        ui->pushButtonStart->setEnabled(false);
        ui->pushButtonStop->setEnabled(true);
    }
    
}

// ################################
// ################################
// ################################

MainWindow::~MainWindow()
{
    delete ui;
}

