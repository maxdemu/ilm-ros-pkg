#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    
    //Init ROS
    int zero = 0;
    ros::init(zero, 0, "talker");
    ros::NodeHandle node;
    _publisher = node.advertise<std_msgs::String>("chatter", 1000);
}

void MainWindow::slotButtonTalk()
{
	//get text from gui
	QString qtext = this->ui->lineEditText->text();
	std::string text = qtext.toStdString();
	
	//compose message
	std_msgs::String message;
	message.data = text;
	
	//publish message
	_publisher.publish(message);
	std::cout << "Published: " << text << std::endl;
}

MainWindow::~MainWindow()
{
    delete ui;
}

