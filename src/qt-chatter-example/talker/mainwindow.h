#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ros/ros.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    
public slots:
	void slotButtonTalk();

private:
    Ui::MainWindow *ui;
    ros::Publisher _publisher;
};
#endif // MAINWINDOW_H
